+++
title = "Privacy Friendly Posting"
date = 2020-06-14T03:21:23-04:00
author = "michelle"
draft = false 
tags = ["links"]
categories = ["guides"]
skip = true
+++

## Best Practices for Linking in NTH Chat

1. Never link directly to executables. Link to a web page instead.

2. **Shortened links**: In most cases we should try not to post "shortened" links, and be wary of clicking on obfuscated links in general. An exception is when a page is loaded with trackers, we can choose to use a cached version of the page at `archive.is` or similar instead.

Protip: checking shortened links before you click is a good idea anyway. For example, to check a shortened link of ```http://tinyurl.com/2g9mqh```, you can use ```curl``` as follows:

```bash
$ curl https://unshorten.me/s/tinyurl.com/2g9mqh
https://www.youtube.com/watch?v=oHg5SJYRHA0
```
or use an online service like [unshorten.it](https://unshorten.it)

3. **Twitter**: For Twitter content please consider using *nitter.net* instead, simply replace ```twitter.com``` with ```nitter.net``` in the URL.  

4. **Youtube**: It's courteous to non-Google fans to link to ```invidio.us``` or similar service instead of Youtube.  

For a standard Youtube link, simply replace ```youtube.com``` with ```invidio.us```. So instead of using ```https://www.youtube.com/watch?v=oHg5SJYRHA0``` you would use instead: ```https://invidio.us/watch?v=oHg5SJYRHA0```.  

5. Please try to remove tracking parameters from the end of a URL. Tracking parameters are part of a querystring, meaning that they come __after__ the question mark in an URL. Not all parameters will be for tracking, sometimes they are needed. But obvious examples indicate the origin of the link, like ```utm_source=twitter```  

6. Please avoid linking to sites that track users if it's unneccessary. For example, if the user is not going to edit a document, then we should avoid linking to a Google Docs page. If the user is simply going to read what is there, consider posting it to a pastebin site, or simply copying the text and posting it in chat instead.  

7. Feel free to offer onion links as alternatives along with their clearnet counterparts. A few people might click those, and using onion links encourages site operators to use them more often. [DuckDuckGo](http://3g2upl4pq6kufc4m.onion), [NYTimes](https://www.nytimes3xbfgragh.onion/), [Invidio.us](http://4l2dgddgsrkf2ous66i6seeyi6etzfgrue332grh2n7madpwopotugyd.onion/), etc. all mirror their sites on onion domains. Some even have [i2p sites](http://owxfohz4kjyv25fvlqilyxast7inivgiktls3th44jhk3ej3i7ya.b32.i2p/), but you get the idea.

